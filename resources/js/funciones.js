window.onload = function() {
  cargarEditar();
};

function cargarPages(pg){
    if(pg == 1){
        document.getElementById("editarContenido").style.display = "inline";
    }else{
        document.getElementById("editarContenido").style.display = "none";
    }
    if(pg == 2){
        document.getElementById("editarGaleria").style.display = "inline";
    }else{
        document.getElementById("editarGaleria").style.display = "none";
    }
}

function cargarEditar(){
    /*if(pagina == 3){
    document.getElementById("editarContenido").style.display = "inline";
    }else{
        if(pagina == 4){
            document.getElementById("editarGaleria").style.display = "inline";
        }
    }*/
}

function cargarPagina(){
    if(document.getElementById("cmb_pages").value != 0){
        var pagina = document.getElementById("cmb_pages").value;
        var data = pagina.split('~');
        document.getElementById("id_pagina").value = data[0];
        document.getElementById("txt_titulo").value = data[1];
        document.getElementById("txt_nombre").value = data[2];
        document.getElementById("txt_contenido").value = data[3];
        if(data[0] == 3){
            document.getElementById("editarContenido").style.display = "inline";
        }else{
            document.getElementById("editarContenido").style.display = "none";
        }
        if(data[0] == 4){
            document.getElementById("editarGaleria").style.display = "inline";
        }else{
            document.getElementById("editarGaleria").style.display = "none";
        }
    }else{
        document.getElementById("id_pagina").value = null;
        document.getElementById("txt_file").value = "";
        document.getElementById("txt_titulo").value = "";
        document.getElementById("txt_nombre").value = "";
        document.getElementById("txt_contenido").value = "";
        document.getElementById("editarContenido").style.display = "none";
    }
}
function cargar()
{
    var table = document.getElementById("tabla"),rIndex;

    for(var i = 1; i< table.rows.length; i++)
    {
        table.rows[i].onclick = function()
        {
            rIndex = this.rowIndex;
            document.getElementById("txt_nombre").value = this.cells[0].innerHTML;
            document.getElementById("txt_apellido").value = this.cells[1].innerHTML;
            document.getElementById("txt_usu").value = this.cells[2].innerHTML;
            document.getElementById("txt_usu").disabled = true;
            document.getElementById("txt_correo").value = this.cells[3].innerHTML;
            /*document.getElementById("txt_contra").disabled = true;
            document.getElementById("txt_contra").type = "text";
            document.getElementById("txt_contra").value = document.getElementById("contra").value;*/
        };
    }
}
function setIdUsuarios(id){
    document.getElementById("id").value = id;
}
function cargarServicios(id)
{
    var table = document.getElementById("tablas"),rIndex;

    for(var i =0; i< table.rows.length; i++)
    {
        table.rows[i].onclick = function()
        {
            rIndex = this.rowIndex;
            document.getElementById("txt_nombres").value = this.cells[0].innerHTML;
            document.getElementById("txt_titulos").value = this.cells[1].innerHTML;
            document.getElementById("txt_descripcions").value = this.cells[2].innerHTML;
        };
    }
}
function limpiarUsuario()
{
    document.getElementById("txt_nombre").value = "";
    document.getElementById("txt_apellido").value = "";
    document.getElementById("txt_usu").value = "";
    document.getElementById("txt_contra").value = "";
    document.getElementById("txt_contra").disabled = false;
    document.getElementById("txt_usu").disabled = false;
    document.getElementById("txt_correo").value = "";
    document.getElementById("input_img").value = "";
    document.getElementById("id").value = "";
    document.getElementById("txt_contra").type = "password";
}
function limpiarVar()
{
   document.getElementById("id").value = ""; 
}
function setIdServicios(id){
    document.getElementById("ser_id").value = id;
    console.log("id ser "+id);
}

function limpiar(){
    document.getElementById("txt_nombres").value = "";
    document.getElementById("txt_titulos").value = "";
    document.getElementById("txt_descripcions").value = "";
    document.getElementById("ser_id").value = null;
}

function limpiarImg(){
    document.getElementById("descripcion_img").value = "";
    document.getElementById("img_id").value = null;
}



