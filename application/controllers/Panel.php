<?php 
class Panel extends CI_Controller 
{

	function __construct()
    {
        parent::__construct();
        $this->load->library('session');

    }

	function index()
	{
		$data['_view'] = 'panel/index';
    	$this->load->view('layouts/main',$data);
	}
}
?>