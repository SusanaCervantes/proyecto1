<?php 
class Editar extends CI_Controller {

	function __construct()
    {
        parent::__construct();
        $this->load->model('Edit_model');
        $this->load->helper('form');
        $this->input->post();
        $this->load->library("session");
    }

	function editar()
    {
        $this->load->library("session");
        $data['servicios'] = $this->Edit_model->getServices();
        $data['paginas'] = $this->Edit_model->getPages();
        $data['galeria'] = $this->Edit_model->getImages();
        $data['_view'] = 'paginas/edit';
        $this->load->view('layouts/main',$data);
    }
    
    function cargar($p){
        $data['edit'] = $p;
        $data['servicios'] = $this->Edit_model->getServices();
        $data['paginas'] = $this->Edit_model->getPages();
        $data['galeria'] = $this->Edit_model->getImages();
        $data['_view'] = 'paginas/edit';
        $this->load->view('layouts/main',$data);
    }
    
    function guardar()
    {   
        $this->load->library('form_validation');
		$this->form_validation->set_rules('txt_titulo', "Titulo",'required|max_length[100]');
        $this->form_validation->set_rules('txt_nombre', "Nombre", 'required|max_length[100]');
        $this->form_validation->set_rules('txt_contenido', "Contenido",'max_length[1000]');
        
        if($this->form_validation->run()){
            if($this->input->post('id_pagina') != null){ 
                $file = $this->banner();
                if($file ==  null){                                        
                    $params = array(
                        'id_page' => $this->input->post('id_pagina'),
                        'page_title' => $this->input->post('txt_titulo'),
                        'page_name' => $this->input->post('txt_nombre'), 
                        'page_content' => $this->input->post('txt_contenido'),
                    );
                    $this->Edit_model->editPage($params);
                }else{
                    $params = array(
                        'id_page' => $this->input->post('id_pagina'),
                        'page_title' => $this->input->post('txt_titulo'),
                        'page_name' => $this->input->post('txt_nombre'), 
                        'page_content' => $this->input->post('txt_contenido'),
                        'page_banner' => $file,
                    );
                    $this->Edit_model->editPageFile($params);
                }
            }else{
                $file = $this->banner();
                if($file != null){
                    $params = array(
                        'id_page' => "/".$this->input->post('txt_nombre'),
                        'page_title' => $this->input->post('txt_titulo'),
                        'page_name' => $this->input->post('txt_nombre'), 
                        'page_content' => $this->input->post('txt_contenido'),
                        'page_banner' => $file,
                    );
                    $this->Edit_model->newPage($params);
                }else{
                    $this->session->set_flashdata('message', 'No se selecciono una imagen');
                }
            }
        }else{
            $this->session->set_flashdata('message', 'Espacios requeridos, falta información');
        }
        redirect('Editar/editar');
    }
    
    public function getPagina($id)
    {
        $page = $this->Edit_model->getPage($id);
        return $page[0];
    }
    
    function banner()
    {
        $config['upload_path']          = './resources/banner/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 5000;
        $config['overwrite']            = true;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('txt_file') )
        {
            $error = array('error' => $this->upload->display_errors());
            return null;
        }
        else
        {
            $data = array('upload_data' => $this->upload->data());
            $params = array(
                'upload' => $this->upload->data('file_name'),
            );

            return $this->upload->data('file_name');
        }
    }
    
    function guardarServicio()
    {
        $this->load->library('form_validation');
		$this->form_validation->set_rules('txt_titulo', "Titulo",'required|max_length[50]');
        $this->form_validation->set_rules('txt_nombre', "Nombre",'required|max_length[1000]');
        $this->form_validation->set_rules('txt_descripcion', "Descripcion",'required|max_length[1000]');
        
        if($this->form_validation->run()){
            echo "Entro";
            if($this->input->post('ser_id') != null){
                $params = array(
                        'ser_title' => $this->input->post('txt_nombre'),
                        'ser_informacion' => $this->input->post('txt_titulo'),
                        'ser_descripcion' => $this->input->post('txt_descripcion'), 
                        'ser_id' => $this->input->post('ser_id'),
                    );
                echo $this->Edit_model->editarServicio($params);
            }else{
                $params = array(
                        'ser_title' => $this->input->post('txt_titulo'),
                        'ser_informacion' => $this->input->post('txt_nombre'),
                        'ser_descripcion' => $this->input->post('txt_descripcion'), 
                    );
                $this->Edit_model->guardarServicio($params);
            }
        }else{
            $this->session->set_flashdata('message', 'Espacios requeridos, falta información');
        }
        $this->session->set_flashdata('pg', '1');
        redirect('Editar/editar');
    }
    
    function eliminarServicio(){
       if(isset($_POST['elm'])){
            $id = $_POST['elm'];
            $this->Edit_model->eliminarServicio($id);
        }
        $this->session->set_flashdata('pg', '1');
        redirect('Editar/editar');
    }
    
    function galeria()
    {
        $config['upload_path']          = './resources/galeria/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 5000;
        $config['overwrite']            = true;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('img') )
        {
            $error = array('error' => $this->upload->display_errors());
            return null;
        }
        else
        {
            $data = array('upload_data' => $this->upload->data());
            $params = array(
                'upload' => $this->upload->data('file_name'),
            );
            return $this->upload->data('file_name');
        }
    }
    
    function guardarGaleria(){
        $this->load->library('form_validation');
        //$this->form_validation->set_rules('img', "Imagen",'required');
        $this->form_validation->set_rules('description_img', "Descripcion",'required|max_length[250]');
        
        if($this->form_validation->run()){
            if($this->Edit_model->getNumImages() == 10){
                $this->session->set_flashdata('message', 'Número máximo de imágenes 10');
            }else{
                $imagen = $this->galeria();
                if($imagen != null){
                    $params = array(
                            'image_description' => $this->input->post('description_img'), 
                            'image' => $imagen,
                        );
                    $this->Edit_model->guardarImagen($params);
                }else{ 
                    $this->session->set_flashdata('message', 'No se selecciono una imagen para subir');
                }
            }
        }else{
            $this->session->set_flashdata('message', 'Espacios requeridos, falta información');
        }
        $this->session->set_flashdata('pg', '2');
        redirect('Editar/editar'); 
    }
    
    function eliminarImagen(){
       if(isset($_POST['elmImg'])){
            $id = $_POST['elmImg'];
            $this->Edit_model->eliminarImagen($id);
        }
        $this->session->set_flashdata('pg', '2');
        $this->cargar(2);
    }

}
?>