<?php 
class Inicio extends CI_Controller {

	function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Inicio_model');
    }

	function index()
    {
        $data['paginas'] = $this->Inicio_model->getPages();
        $c = $this->Inicio_model->getPage();
        $data['page'] = $c[0];
        $data['_view'] = 'paginas/index';
        $this->load->view('layouts/main',$data);
    }
    
    function getContent($nombre)
    {
        current_url();
        $data['paginas'] = $this->Inicio_model->getPages();
        $data['page'] = $this->Inicio_model->getPageByName($nombre);
        $data['_view'] = 'paginas/index';
        $this->load->view('layouts/main',$data);
    }

    function cargarGaleria()
    {
        current_url();
        $data['paginas'] = $this->Inicio_model->getPages();
        $data['page'] = $this->Inicio_model->getPageByName(4);
        $data['imagenes'] = $this->Inicio_model->getGalery();
        $data['_view'] = 'paginas/galery';
        $this->load->view('layouts/main',$data);
    }
}
?>