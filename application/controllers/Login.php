<?php 
class Login extends CI_Controller {

	function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
        $this->load->library('form_validation');
		$this->load->library('session');
		$this->load->model('Login_model');
    }

	function index()
	{
		$data['_view'] = 'login/index';
    	$this->load->view('layouts/main',$data);
	}
	public function login() 
	{

		$this->form_validation->set_rules('txt_usuario', 'Username', 'trim|required|xss_clean');
		$this->form_validation->set_rules('txt_contra', 'Password', 'trim|required|xss_clean');

		if ($this->form_validation->run() == FALSE) 
		{ 
			if(isset($this->session->userdata['logged_in']))
			{
				$this->session->set_flashdata('message', 'Usuario y contraseña requeridos, verifique');
            	redirect('Login/index');
			}
		} 
		else 
		{

			$data = array(
				'username' => $this->input->post('txt_usuario'),
				'password' => $this->input->post('txt_contra')
			);

			$result = $this->Login_model->login($data);

			if ($result == TRUE) 
			{ 

				$username = $this->input->post('txt_usuario');
				$password = $this->input->post('txt_contra');
				$result = $this->Login_model->get_user_information($username,$password);

				if ($result != false) {
					$session_data = array(
						'logged_in' => TRUE,
						'id_user' => $result[0]->id_user,
						'username' => $result[0]->username,
						'name' => $result[0]->name,
						'lastname' => $result[0]->lastname,
						'email' => $result[0]->email,
						'photo' => $result[0]->photo,
					);
					$this->session->set_userdata('logged_in', $session_data);
					redirect('panel/index', 'refresh'); 
					$this->load_data_view('panel/index'); 

				}
			} 
			else 
			{ 
				$this->session->set_flashdata('message', 'Espacios requeridos, falta información');
            	redirect('Login/index');
			}
		}
	}

}
?>