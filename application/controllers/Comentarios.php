<?php 
class Comentarios extends CI_Controller 
{

	function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Contacto_model');
    }

	function index($comentarios = false)
	{
		$data['_view'] = 'comentarios/index';
        $data['comentarios'] = $comentarios ? $comentarios : $this->Contacto_model->obtenerComentarios(); 
    	$this->load->view('layouts/main',$data);
	}
}
?>