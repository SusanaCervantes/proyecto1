<?php

class Contacto extends CI_Controller
{
	function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Contacto_model');
        $this->load->model('Inicio_model');
    }

	function index()
	{
        current_url();
        $data['paginas'] = $this->Inicio_model->getPages();
        $data['page'] = $this->Inicio_model->getPageByName(5);
		$data['_view'] = 'contacto/index';
    	$this->load->view('layouts/main',$data);
	}
	function enviar()
	{
		$this->load->library('form_validation');

        $this->form_validation->set_rules('txt_nom','Nombre','required|max_length[64]');
        $this->form_validation->set_rules('txt_cor','Correo','required|max_length[64]');
        $this->form_validation->set_rules('txt_com','Comentario','required|max_length[150]');

        if($this->form_validation->run())
        {
        	$params = array(
            'comment' => $this->input->post('txt_com'),
            'name' => $this->input->post('txt_nom'),
            'email' => $this->input->post('txt_cor'),
            );
            $this->Contacto_model->enviarComentario($params);
        }
        else
        {
        	echo "error";
        }
        redirect('contacto/index');
	}
}

















?>