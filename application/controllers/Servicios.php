<?php 
class Servicios extends CI_Controller {

	function __construct()
    {
        parent::__construct();
     	$this->load->model('Servicios_model');
     	$this->load->model('Inicio_model');

    }

	function index(){
	$data['busqueda']="no";
	$data['message'] = $this->Servicios_model->getAll();
	$data['paginas'] = $this->Inicio_model->getPages();
	$data['page'] = $this->Inicio_model->getPageByName('3');
	$data['_view'] = 'servicios/index';
    $this->load->view('layouts/main',$data);
	}

	function vermas($ruta){
	$data['busqueda']="si";
	$data['message'] = $this->Servicios_model->getService($ruta);
	$data['paginas'] = $this->Inicio_model->getPages();
	$data['page'] = $this->Inicio_model->getPageByName('3');
	$data['_view'] = 'servicios/index';
    $this->load->view('layouts/main',$data);
	}

	function atras(){
		redirect('servicios/'); 
	}
}
 ?>