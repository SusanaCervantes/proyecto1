<?php 
class Usuarios extends CI_Controller {

	function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Usuarios_model');
    }

	function index($usuarios = false)
	{
		$data['_view'] = 'usuario/index';
        $data['usuarios'] = $usuarios ? $usuarios : $this->Usuarios_model->obtenerUsuarios(); 
    	$this->load->view('layouts/main',$data);
	}
	function guardar()
	{
        $iv = "1234567812345678";

		$this->load->library('form_validation');

        $this->form_validation->set_rules('txt_nombre','Nombre','required|max_length[64]');
        $this->form_validation->set_rules('txt_apellido','Apellido','required|max_length[64]');
       // $this->form_validation->set_rules('txt_contra','Contraseña','required|max_length[128]');
        $this->form_validation->set_rules('txt_correo','Correo','required|max_length[64]');
       // $this->form_validation->set_rules('txt_usu','Contraseña','required|max_length[128]');

        if($this->form_validation->run())
        {
            if($this->input->post('id') != null)
            {
                $foto = $this->foto();
                if($foto == null)
                {
                    $params = array(
                    'id_user' => $this->input->post('id'),
                    'name' => $this->input->post('txt_nombre'),
                    'password' =>$this->input->post('txt_contra'),
                    'lastname' => $this->input->post('txt_apellido'),
                    'email' => $this->input->post('txt_correo'),
                    );
                    $this->Usuarios_model->editarUsuarioSinFoto($params);
                }
                else
                {
                    $params = array(
                    'id_user' => $this->input->post('id'),
                    'name' => $this->input->post('txt_nombre'),
                    'password' => $this->input->post('txt_contra'),
                    'lastname' => $this->input->post('txt_apellido'),
                    'email' => $this->input->post('txt_correo'),
                    'photo' => $foto,
                    );
                    $this->Usuarios_model->editarUsuario($params);
                }    
            }
            else
            {
                $foto = $this->foto();
                if($foto != null)
                {
                    $params = array(
                    'name' => $this->input->post('txt_nombre'),
                    'username' => $this->input->post('txt_usu'),
                    'password' => $this->input->post('txt_contra'),
                    'lastname' => $this->input->post('txt_apellido'),
                    'email' => $this->input->post('txt_correo'),
                    'photo' => $foto,
                    );
                    $this->Usuarios_model->guardarUsuario($params);
                }
            }
        }
        else
        {
            $this->session->set_flashdata('message', 'Campos requeridos, verifique');
        }

        redirect('usuarios/index');
	}
    function eliminar($iduser)
    {
        $data['user'] = $this->Usuarios_model->obtenerUsuario($iduser);
        $this->Usuarios_model->eliminarUsuario($iduser);
        $this->index();
    }
    function buscar()
    {
        $this->Usuarios_model->buscarUsuario();
    }
    function foto()
    {
        $config['upload_path']          = './resources/photos/';
        $config['allowed_types']        = 'jpg|png|jpeg';
        $config['max_size']             = 5000;
        $config['overwrite']            = true;

        $this->load->library('upload', $config);
        if( ! $this->upload->do_upload('input_img'))
        {
            $error = array('error' => $this->upload->display_errors());
            //echo $error['error'];
            return null;
        }
        else
        {
            $data = array('upload_data' => $this->upload->data());
            $params = array(
                'photo' => $this->upload->data('file_name'),
            );

            return $this->upload->data('file_name');
        }
    }
}
?>