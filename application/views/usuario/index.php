<!DOCTYPE html>
<html>
<head>
	<title>Usuarios</title>
</head>


<body>
	<nav class="navbar navbar-light" style="background-color: #F5F5F5;">
   		 <div class="container">
    		<ul class="nav navbar-nav">
			
				<li class="nav-item"><a style="color: #616161;" href=<?php echo base_url() . "Editar/editar"?>>Editar Secciones</a></li>
				<li class="nav-item"><a style="color: #616161;" href=<?php echo base_url() . "usuarios/index"?>>Agregar o Editar Usuarios</a></li>
				<li class="nav-item"><a style="color: #616161;" href=<?php echo base_url() . "comentarios/index"?>>Comentarios</a></li>
				<li class="nav-item"><a style="color: #616161;" href=<?php echo base_url() . "login/index"?>>Salir</a></li>
				</ul>
		</div>
	</nav>
	<?php
    if($this->session->flashdata('message')) {
        echo "<div class='alert alert-info' role='alert'>". $this->session->flashdata('message') ."</div>" ;
    }?>
	<div >
		<?php echo "<img id='img_foto' src='". site_url('/resources/photos/' . $this->session->userdata['logged_in']['photo'])."'>"?>
		<?php echo "<label id='lbl_nom'>". $this->session->userdata['logged_in']['name']."</label>" ?>
	</div>
	<?php echo form_open_multipart('usuarios/guardar');?>
	<div id="form">
		<input type="text" name="txt_nombre" id="txt_nombre" class="txt" placeholder="Nombre">
		<input type="text" name="txt_apellido" id="txt_apellido" class="txt" placeholder="Apellidos">
		<input type="text" name="txt_usu" id="txt_usu" class="txt" placeholder="Usuario">
		<input type="password" name="txt_contra" id="txt_contra" class="txt" placeholder="Contraseña">
		<input type="text" name="txt_correo" class="txt" id="txt_correo" placeholder="Correo">
		<input type="file" id="input_img" name="input_img" style="margin: 10px 0px 0px 0px;">
		<input type="hidden" name="id" id="id" value="">
		<div style="margin: 20px 0px 0px 590px;"><button type="submit" class="boton" onclick="">Guardar</button></div>
	</div>
	<?php echo form_close(); ?>

	<div style="margin: -300px 0px 0px 890px;">
		<button class="boton" onclick="limpiarUsuario()">Limpiar</button>
	</div>
	

	<div id="tb">
		<table id="tabla" onclick="cargar()">
			<thead><td>Nombre</td><td>Apellido</td><td>Usuario</td><td>Correo</td><td>Borrar</td></thead>
			<?php foreach($usuarios as $u) { ?>
			<tbody onclick="setIdUsuarios(<?php echo $u['id_user'] ?>)" id="body">
				<tr><td><?php echo $u['name'] ?></td><td><?php echo $u['lastname'] ?></td><td><?php echo $u['username'] ?></td><td><?php echo $u['email'] ?></td>
					<td><a href="<?php echo site_url('usuarios/eliminar/' . $u['id_user']); ?>">🗙</a></td></tr>
			</tbody>
			<input type="hidden" name="contra" id="contra" value=<?php echo $u['password'] ?>>
				<?php } ?>
				
		</table>
		
	</div>
</body>

</html>