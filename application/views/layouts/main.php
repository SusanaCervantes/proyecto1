<!DOCTYPE HTML>
<html>
  <head>
    <link rel="icon" href="<?php echo site_url('resources/img/favicon.png');?>" type="image/x-icon">
    <link rel="stylesheet" href="<?php echo site_url('resources/css/style.css');?>">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script type="text/javascript" src="<?php echo site_url('resources/js/funciones.js');?>"></script>
   <title>Proyecto1</title>
  </head>

  <body>
  	<div>
	    <?php                    
	    if(isset($_view) && $_view)
	        $this->load->view($_view);
	    ?> 
    </div>
    <!--<footer style="background-color: #E1F5FE;">
      <p class="text-center" style="margin-top: 5%; color:#BDBDBD; position: absolute; bottom: 0; width: 100%;"> &copy; Proyecto Programación IV <br> UNA 2020</p>
    </footer>-->
  </body>
</html>