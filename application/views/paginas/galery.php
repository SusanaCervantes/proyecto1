<div class="imgPortada center">
    <img class="portada" src="<?php echo site_url('/resources/banner/' . $page['page_banner']); ?>">
</div>
<nav class="navbar navbar-light" style="background-color: #e3f2fd;">
    <div class="container">
        <ul class="nav navbar-nav">
            <li class="nav-item active"><a style="color: #616161;" href="/Proyecto1">Inicio</a></li>
            <?php foreach($paginas as $p){ ?>
                <?php if($p['id_page'] < 6){ ?>
                    <li class="nav-item"><a style="color: #616161;" href=<?php echo base_url() . $p['page_url'] ?> ><?php echo $p['page_name']; ?></a></li> 
                <?php }else{ ?>
                <li class="nav-item"><a style="color: #616161;" href="<?php echo base_url() . 'Inicio/getContent/' . $p['id_page']; ?>"><?php echo $p['page_name']; ?></a></li>
                <?php } ?>
            <?php } ?>
        </ul>
    </div>
</nav>

<div class="contenido">
    <div>
        <h3 class="titulo"> <?php echo $page['page_title']; ?> </h3>  <!-- Titulo de la pagina -->
    </div>
    <div>
        <p class="detalle">
            <?php echo $page['page_content']; ?> <!-- Aqui va el contenido de la pagina -->
        </p>
    </div>
    
        <div id="carrusel" class="carousel slide" data-ride="carousel" style="margin-bottom: 100px;">
            <div class="carousel-inner"  role="listbox">
                <?php $c = 1; ?>
                <?php foreach($imagenes as $i){ ?>
                <?php if($c == 1){ ?>
                    <div class="item active">
                        <div class="view">
                        <img class="d-block w-100" style="width:100%;" src="<?php echo site_url('/resources/galeria/' . $i['image']); ?>">
                        <div class="mask rgba-black-light"></div>
                        </div>
                        <div class="carousel-caption">
                            <h3 class="h3-responsive"><?php echo $i['image_description']; ?></h3>
                        </div>
                    </div>
                <?php }else{ ?>
                    <div class="item">
                        <div class="view">
                        <img class="d-block w-100" style="width:100%;" src="<?php echo site_url('/resources/galeria/' . $i['image']); ?>">
                        <div class="mask rgba-black-light"></div>
                        </div>
                        <div class="carousel-caption">
                            <h3 class="h3-responsive"><?php echo $i['image_description']; ?></h3>
                        </div>
                    </div>
                <?php } ?>
                <?php $c = 0; } ?>
            </div>
            <a class="left carousel-control" href="#carrusel" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
                <span class="sr-only">Anterior</span>
            </a>
            <a class="right carousel-control" href="#carrusel" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
                <span class="sr-only">Siguiente</span>
            </a>
        </div>
    
</div>
