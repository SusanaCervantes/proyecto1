<div class="edit">
    <nav class="navbar navbar-light" style="background-color: #F5F5F5;">
         <div class="container">
            <ul class="nav navbar-nav">
            
                <li class="nav-item"><a style="color: #616161;" href=<?php echo base_url() . "Editar/editar"?>>Editar Secciones</a></li>
                <li class="nav-item"><a style="color: #616161;" href=<?php echo base_url() . "usuarios/index"?>>Agregar o Editar Usuarios</a></li>
                <li class="nav-item"><a style="color: #616161;" href=<?php echo base_url() . "comentarios/index"?>>Comentarios</a></li>
                <li class="nav-item"><a style="color: #616161;" href=<?php echo base_url() . "login/index"?>>Salir</a></li>
                </ul>
        </div>
    </nav>
    <?php
    if($this->session->flashdata('message')) {
        echo "<div class='alert alert-info' role='alert'>". $this->session->flashdata('message') ."</div" ;
    }?>
    <label>Editar seccion</label>
    <div>
        <br>
        <label>Pagina a editar</label>
        <select id="cmb_pages" onchange="cargarPagina()" class="txt">
            <option value="0">Nueva</option>
            <?php foreach($paginas as $page){ ?>
                <option value="<?php echo implode('~', $page); ?>"><?php echo $page['page_title']; ?></option>
            <?php } ?>
        </select>
    </div>
    <br>
    <div style="display: flex;">
        <div id="page_content" class="edit_sec1">
            <?php echo form_open_multipart('Editar/guardar/');?>
            <div>
                <label>Imagen</label>
                <input type="file" id="txt_file" name="txt_file" accept="image/jpeg,image/gif,image/png">
            </div>
            <br>
            <div>
                <label>Titulo:</label>
                <input type="text" id="txt_titulo" name="txt_titulo" style="border-radius: 10px;">
            </div>
            <div>
                <label>Nombre:</label>
                <input type="text" id="txt_nombre" name="txt_nombre" style="border-radius: 10px;">
            </div>
            <div>
                <label>Detalle:</label>
                <textarea id="txt_contenido" name="txt_contenido" rows="10" cols="50" style="border-radius: 10px;"></textarea>
            </div>
            <input type="hidden" id="id_pagina" name="id_pagina">
            <button type="submit" id="btn_guardar" style="border-radius: 10px;">Guardar</button>
            <span style="color: #f00"><?php echo form_error('txt_contenido');?></span>
            <?php echo form_close();?> 
        </div>
        
        <div id="editarContenido" style="display:block;"> <!--Donde se agregarian los editar de servicios y galeria-->
            <script> cargarPagina(); </script>
             <div class="edit_sec">       
                <div id="form1">
                    <?php echo form_open("Editar/guardarServicio");?>
                    <input type="text" name="txt_nombre" value="" id="txt_nombres" class="txt" placeholder="Titulo">
                    <input type="text" name="txt_descripcion" value="" id="txt_descripcions" class="txt" placeholder="Descripción">
                    <br>
                    <textarea name="txt_titulo" value="" rows="2" cols="66" id="txt_titulos" style="border-radius: 10px;" placeholder="Informacion"></textarea>
                    <input type="hidden" name="ser_id" id="ser_id">
                    <br>
                    <span style="color: #f00"><?php echo form_error('txt_titulo');?></span>
                    <button type="submit" style="border-radius: 10px;">Guardar</button>
                    <button  type="button" onclick="limpiar()" class="fa fa-plus" style="border-radius: 10px;"></button>
                    <?php echo form_close();?>
                    
                </div>
                <div style="margin: -520px 0px 0px 40px;">
                </div>
                <div id="tb1">
                    <table id="tablas">
                        <thead><td>Titulo</td><td>Informacion</td><td>Descripción</td><td>X</td></thead>
                            <?php foreach($servicios as $s) { ?> 
                            <tbody id="body" onclick="setIdServicios(<?php echo $s["ser_id"]; ?>)">
                            <form action='eliminarServicio' id='eliminar_<?php echo $s["ser_id"]; ?>' method="post"></form>
                            <input type="hidden" name="elm" value="<?php echo $s["ser_id"]; ?>" form='eliminar_<?php echo $s["ser_id"]; ?>'>
                            <tr onclick="cargarServicios(<?php echo $s["ser_id"]; ?>)"><td><?php echo $s["ser_title"]; ?></td><td><?php echo $s["ser_informacion"]; ?></td><td><?php echo $s["ser_descripcion"] ?></td><td><button name="btn_eliminar" type="submit" form='eliminar_<?php echo $s["ser_id"]; ?>'><i class='fa fa-trash'></i></button></td></tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div> 
        </div>
        
        <div id="editarGaleria" style="display:none;"> <!--Donde se agregarian los editar de servicios y galeria-->
             <div class="edit_sec">       
                <div id="form1">
                    <?php echo form_open_multipart("Editar/guardarGaleria");?>
                    <textarea name="description_img" value="" rows="2" cols="85" id="description_img" style="border-radius: 10px;" placeholder="Descripcion"></textarea>
                    <br>
                    <div style="display: inline-flex;">
                        <input type="file" name="img" id="img" accept="image/jpeg,image/gif,image/png">
                        <button type="submit" style="border-radius: 10px;">Guardar</button>
                    </div>
                    <span style="color: #f00"><?php echo form_error('description_img');?></span>
                    <?php echo form_close();?>
                </div>
                <div style="margin: -520px 0px 0px 40px;">
                </div>
                <div id="tb2" class="center">
                    <table id="imgs">
                        <thead><td>Imagen</td><td>Descripcion</td><td>Eliminar</td></thead>
                            <?php foreach($galeria as $img) { ?> 
                                <tbody>
                                <form action='eliminarImagen' id='img_<?php echo $img["image_id"]; ?>' method="post"></form>
                                <input type="hidden" name="elmImg" value="<?php echo $img["image_id"]; ?>" form='img_<?php echo $img["image_id"]; ?>'>
                                <tr><td><img style="max-width: 70%; max-height: 70%;" src="<?php echo site_url('/resources/galeria/' . $img["image"]); ?>"></td><td><?php echo $img["image_description"]; ?></td><td><div class="center"><button name="btn_eliminar" type="submit" form='img_<?php echo $img["image_id"]; ?>'><i class='fa fa-trash'></i></button></div></td></tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div> 
        </div>
        <?php
        if($this->session->flashdata('pg')){ ?>
            <script> cargarPages(<?php echo $this->session->flashdata('pg') ?>); </script>
        <?php } ?>
    </div>
</div>