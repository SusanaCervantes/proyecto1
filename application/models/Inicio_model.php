<?php

class Inicio_model extends CI_Model
{
	function __construct()     
	{        
	 	parent::__construct();     
	}
    
    function getPage(){
      return $this->db->query("SELECT pages.page_title, pages.page_content, pages.page_banner
                FROM pages
                WHERE id_page = 1")->result_array();
    }
    
    function getPages(){
        return $this->db->query("SELECT id_page, page_title, page_name, page_content, page_banner, pages.page_url
                FROM pages WHERE id_page != 1")->result_array();
    }
    
    function getPageByName($nombre){
        return $this->db->query("SELECT pages.page_title, page_name, pages.page_content, pages.page_banner
                FROM pages
                WHERE id_page = '". $nombre . "'")->row_array();
    }
    
    function getGalery(){
        return $this->db->query("SELECT image, image_description
                FROM galery ")->result_array();
    }
}