<?php 

class Contacto_model extends CI_Model
{
	
	function __construct()     
	{        
	 	parent::__construct();     
	}
	function enviarComentario($params)
	{
		$this->db->insert('contact',$params);         
		return $this->db->insert_id();
	}
	function obtenerComentarios()
	{
		return $this->db->query("SELECT * FROM contact ORDER BY id_contact DESC")->result_array();
	}
	
}
?>