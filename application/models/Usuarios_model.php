<?php

class Usuarios_model extends CI_Model
{
	function __construct()     
	{        
	 	parent::__construct();     
	} 
	function obtenerUsuario($iduser)
	{
		return $this->db->query("SELECT user.* From user WHERE user.id_user = " . $iduser)->row_array();
	}
	function obtenerUsuarios()
	{
		return $this->db->query("SELECT * FROM user ORDER BY id_user DESC")->result_array();
	}
	function guardarUsuario($params)
	{
		$this->db->insert('user',$params);         
		return $this->db->insert_id();
	}
	function eliminarUsuario($iduser)
	{
		return $this->db->delete('user',array('id_user'=>$iduser)); 
	}
	function editarUsuario($params)
	{    
		return $this->db->query("UPDATE user SET user.name = '". $params['name']."', user.lastname ='". $params['lastname'] ."', user.password = '". $params['password'] ."', user.email = '". $params['email'] ."',
		user.photo = '" . $params['photo'] ."' where user.id_user = " . $params['id_user']);      
	}
	function editarUsuarioSinFoto($params)
	{    
		return $this->db->query("UPDATE user SET user.name = '". $params['name']."', user.lastname ='". $params['lastname'] ."', user.password = '". $params['password'] ."', user.email = '". $params['email'] ."' where user.id_user = " . $params['id_user']);      
	}
}
	if(isset($_GET['parametro']))
	{
		buscarUsuario($_GET['parametro']);
	}
?>