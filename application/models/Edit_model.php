<?php

class Edit_model extends CI_Model
{
	function __construct()     
	{        
	 	parent::__construct();     
	}
    
    function getPages(){
        return $this->db->query("SELECT id_page, page_title, page_name, page_content, page_banner
                FROM pages ")->result_array();
    }
    
    function getPage($id){
        return $this->db->query("SELECT id_page, page_title, page_name, page_content, page_banner
                FROM pages 
                WHERE id_page = ". $id)->result_array();
    }
    
    function editPage($params){ 
        $contenido = $this->db->escape_str($params['page_content']);
        return $this->db->query("UPDATE pages SET pages.page_title = '". $params['page_title'] ."', pages.page_name = '". $params['page_name'] ."', pages.page_content = '". $contenido ."' WHERE pages.id_page = " . $params['id_page']);
    }
    
    function editPageFile($params){
        $contenido = $this->db->escape_str($params['page_content']);
        return $this->db->query("UPDATE pages SET pages.page_title = '". $params['page_title'] ."', pages.page_name = '". $params['page_name'] ."', pages.page_content = '". $contenido ."', pages.page_banner = '". $params['page_banner'] ." ' WHERE pages.id_page = " . $params['id_page']);
    }
    
    function newPage($params){
        $this->db->insert('pages',$params);
        return $this->db->insert_id();
    }
    
    function getServices(){
        return $this->db->query("SELECT ser_id, ser_title, ser_informacion, ser_descripcion
                FROM services ")->result_array();
    }
    
    function guardarServicio($params){
        $this->db->insert('services',$params);
        return $this->db->insert_id();
    }
    
    function eliminarServicio($id){
        $this->db->query("DELETE FROM services WHERE ser_id = '$id'");
    }
    
    function editarServicio($params){ 
        return $this->db->query("UPDATE services SET ser_title = '". $params['ser_title'] ."', ser_informacion = '". $params['ser_informacion'] ."', ser_descripcion = '". $params['ser_descripcion'] ."' WHERE ser_id = " . $params['ser_id']);
    }
    
    function getImages(){
        return $this->db->query("SELECT image_id, image, image_description
                FROM galery ")->result_array();
    }
    
    function guardarImagen($params){
        $this->db->insert('galery',$params);
        return $this->db->insert_id();
    }
    
    function eliminarImagen($id){
        $this->db->query("DELETE FROM galery WHERE image_id = '$id'");
    }
    function getNumImages(){
        return $this->db->query("SELECT image_id, image, image_description
                FROM galery ")->num_rows();
    }
}